//PATH TO DOMOTICZ
var _HOST_DOMOTICZ		  	= 'http://192.168.1.3:1234';

/*
IF YOU HAVE A MEDIABOX FROM ZIGGO (HORIZON), COPY SWITCH_HORIZON.PHP ON A WEBSERVER INSIDE YOUR NETWORK AND CHANGE THE IP.
ENTER THE PATH TO THIS FILE BELOW.
*/
var _HOST_ZIGGO_HORIZON	  	= '';//http://192.168.1.3/domoticz/switch_horizon.php';
var _HOST_NZBGET 			= 'http://192.168.1.3:1234';
var _APIKEY_WUNDERGROUND  	= '';
var _WEATHER_CITY 			= 'Eindhoven';
var _WEATHER_COUNTRY 		= 'NL';
var _USE_DUTCH_TRAFFICINFO	= true;
var _USE_DUTCH_TRAININFO	= true;
var _BACKGROUND_IMAGE		= 'bg8.jpg';

var buttons = {}
buttons.radio = {icon: 'fa-play', title: 'Radio', url: 'http://nederland.fm'}
buttons.radar = {icon: 'fa-cloud', title: 'Radar', url: 'http://www.weer.nl/verwachting/nederland/son/189656/'}

var titles = {}
titles.groups = 'Verlichting'; //or false to hide block
titles.lights = '';
titles.forecast = '';
titles.horizon = 'TV';
titles.nzbget = 'Downloads';
titles.traffic = '';
titles.train = '';