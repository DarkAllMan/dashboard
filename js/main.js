var req;
var slide;
var sliding = false;

$(document).ready(function(){
	
	$('header').css('background-image','url(\'img/'+_BACKGROUND_IMAGE+'\')');
	
	setInterval(function(){ 
		$('#clock').html(moment().locale('nl').format('HH:mm:ss'));
		$('#date').html(moment().locale('nl').format('D MMMM YYYY'));
	},100);
	
	getDevices(); 
	setInterval(function(){ 
		getDevices(); 
	}, 5000);
	
	if(_APIKEY_WUNDERGROUND!=="" && _WEATHER_CITY!==""){
		loadWeather(_WEATHER_CITY,_WEATHER_COUNTRY);
			loadWeatherFull(_WEATHER_CITY,_WEATHER_COUNTRY,$('#weatherfull'));
		setInterval(function(){ 
			loadWeather(_WEATHER_CITY,_WEATHER_COUNTRY);
			loadWeatherFull(_WEATHER_CITY,_WEATHER_COUNTRY,$('#weatherfull'));
		}, (60000*30));
	}
	else {
		$('.containsweather').remove();
	}
	
	if(_HOST_ZIGGO_HORIZON==""){
		$('.containshorizon').remove();
	}
	
	loadButtons();
	
	$( ".slider" ).slider({
		formatter: function(value) {
			return 'Current value: ' + value;
		}
	});
	
	if(_USE_DUTCH_TRAFFICINFO){
		getTraffic();
		setInterval(function(){getTraffic();}, (60000*5));
	}
	
	if(_USE_DUTCH_TRAININFO){
		getTrainInfo();
		setInterval(function(){getTrainInfo();}, (60000*5));
	}
	
	$('body').attr('unselectable','on')
     .css({'-moz-user-select':'-moz-none',
           '-moz-user-select':'none',
           '-o-user-select':'none',
           '-khtml-user-select':'none', /* you could also put this in a class */
           '-webkit-user-select':'none',/* and add the CSS class here instead */
           '-ms-user-select':'none',
           'user-select':'none'
     }).bind('selectstart', function(){ return false; });
	
	setTitles();
	
	if(_HOST_NZBGET!==""){
		loadNZBGET(); setInterval(function(){ loadNZBGET(); },5000);
	}
	else $('.containsnzbget').remove();
}); 

function ziggoRemote(key){
	$.get(_HOST_ZIGGO_HORIZON+'?key='+key);
}

function setTitles(){
	if(titles.groups) $('.titlegroups').find('h3').html(titles.groups);
	else $('.titlegroups').hide();
	
	if(titles.lights) $('.titlelights').find('h3').html(titles.lights);
	else $('.titlelights').hide();
	
	if(titles.forecast) $('.titleforecast').find('h3').html(titles.forecast);
	else $('.titleforecast').hide();
	
	if(titles.horizon) $('.titlehorizon').find('h3').html(titles.horizon);
	else $('.titlehorizon').hide();
	
	if(titles.nzbget) $('.titledownloads').find('h3').html(titles.nzbget);
	else $('.titledownloads').hide();
	
	if(titles.traffic) $('.titletraffic').find('h3').html(titles.traffic);
	else $('.titletraffic').hide();
	
	if(titles.train) $('.titletrain').find('h3').html(titles.train);
	else $('.titletrain').hide();
}
function loadButtons(){
	var t=1;
	for(b in buttons){
		var html = '<div class="modal fade" id="'+b+'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
		  html+='<div class="modal-dialog">';
			html+='<div class="modal-content">';
			  html+='<div class="modal-header">';
				html+='<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
				html+='<h4 class="modal-title" id="myModalLabel2">'+buttons['title']+'</h4>';
			  html+='</div>';
			  html+='<div class="modal-body">';
				  html+='<iframe data-src="'+buttons[b]['url']+'" width="100%" height="570" frameborder="0" allowtransparency="true"></iframe> '; 
			  html+='</div>';
			html+='</div>';
		  html+='</div>';
		html+='</div>';
		$('header').append(html);
		
		if(t==1) var classn='no-padding';
		else var classn='no-pr';
		var html='<div class="col-md-6 '+classn+'" data-toggle="modal" data-target="#'+b+'" onclick="setSrc(this);">';
			html+='<div class="transbg">';
				html+='<div class="col-md-4 no-padding">';
					html+='<em class="fa '+buttons[b]['icon']+' fa-small"></em>';
				html+='</div>';
				html+='<div class="col-md-8 no-padding">';
					html+='<strong class="title">'+buttons[b]['title']+'</strong><br>';
					html+='<span class="state"></span>';
				html+='</div>';
			html+='</div>';
		html+='</div>';
		$('#buttons').append(html);
		
		if(t==2) t=1;
		t++;
	}
}

function getDevices(){
	
	if(!sliding){
		if(typeof(req)!=='undefined') req.abort();
		req = $.ajax({
			url: _HOST_DOMOTICZ+'/json.htm?type=devices&filter=all&used=true&order=Name&jsoncallback=?',
			type: 'GET',async: true,contentType: "application/json",dataType: 'jsonp',
			success: function(data) {
				//data = $.parseJSON(jsonexample);
			if(!sliding){
					$('#switches').html('');
					$('#groups').html('');
					$('#sliders').html('');
					for(r in data.result){
						
						if(data.result[r]['HardwareType']=='Toon Thermostat'){
							if(data.result[r]['Type']=='P1 Smart Meter' && data.result[r]['SubType']=='Energy'){
								$('.energy').html(data.result[r]['Usage']);
								$('.energytoday').html(data.result[r]['CounterToday']);
								$('.energytoday').parents('.hideinit').show();
								$('.energy').parents('.hideinit').show();
							}
							if(data.result[r]['Type']=='P1 Smart Meter' && data.result[r]['SubType']=='Gas'){
								$('.gas').html(number_format(data.result[r]['CounterToday'],2,',','.')+' m3');
								$('.gas').parents('.hideinit').show();
							}
							if(data.result[r]['Type']=='Temp'){
								$('.tempinside').html(data.result[r]['Temp']+'°C');
								$('.tempinside').parents('.hideinit').show();
							}
						}

						if(data.result[r]['HardwareName']=='Wunderground' && data.result[r]['Type']=='Rain'){
							if(parseFloat(data.result[r]['RainRate'])>0) $('.rainrate').html(' / '+data.result[r]['RainRate']+'mm');
						}
						
						if(parseFloat(data.result[r]['Favorite'])==1){
							if(data.result[r]['HardwareName']=='Motherboard'){
								$('.block'+data.result[r]['idx']).remove();
								var html='<div class="col-md-12 mb block'+data.result[r]['idx']+'">';
									html+='<div class="transbg">';
										html+='<div class="col-md-4 no-padding">';
											html+='<em class="fa fa-desktop"></em>';
										html+='</div>';
										html+='<div class="col-md-8 no-padding">';
											html+='<strong class="title harddisk">'+data.result[r]['Data']+'</strong><br />';
											html+='<span>'+data.result[r]['Name']+'</span>';
										html+='</div>';
									html+='</div>';
								html+='</div>';
								$('.blocks').append(html);
							}
							else if(data.result[r]['Type']=='Temp' && data.result[r]['SubType']=='LaCrosse TX3'){
								$('.block'+data.result[r]['idx']).remove();
								var switchHTML = '<div class="col-md-4 mb no-pr" data-light="'+data.result[r]['idx']+'">';
										switchHTML+='<div class="transbg"> ';
											switchHTML+='<div class="col-md-4 no-padding">';
												switchHTML+='<em class="fa fa-thermometer-half"></em>';
											switchHTML+='</div>';
											switchHTML+='<div class="col-md-8 no-padding">';
												switchHTML+='<strong class="title">'+data.result[r]['Name']+'</strong><br />';
												switchHTML+='<span class="state">'+data.result[r]['Temp']+'°C</span>';
											switchHTML+='</div>';
										switchHTML+='</div>';
									switchHTML+='</div>';
								$('#switches').append(switchHTML);
							}
							else if(data.result[r]['Type']=='Humidity' && data.result[r]['SubType']=='LaCrosse TX3'){
								$('.block'+data.result[r]['idx']).remove();
								var switchHTML = '<div class="col-md-4 mb no-pr" data-light="'+data.result[r]['idx']+'">';
										switchHTML+='<div class="transbg"> ';
											switchHTML+='<div class="col-md-4 no-padding">';
												switchHTML+='<em class="wi wi-humidity"></em>';
											switchHTML+='</div>';
											switchHTML+='<div class="col-md-8 no-padding">';
												switchHTML+='<strong class="title">'+data.result[r]['Name']+'</strong><br />';
												switchHTML+='<span class="state">'+data.result[r]['Humidity']+'%</span>';
											switchHTML+='</div>';
										switchHTML+='</div>';
									switchHTML+='</div>';
								$('#switches').append(switchHTML);
							}
							else if(data.result[r]['Type']=='General' && data.result[r]['SubType']=='Barometer'){
								$('.block'+data.result[r]['idx']).remove();
								var switchHTML = '<div class="col-md-4 mb no-pr" data-light="'+data.result[r]['idx']+'">';
										switchHTML+='<div class="transbg"> ';
											switchHTML+='<div class="col-md-4 no-padding">';
												switchHTML+='<em class="wi wi-barometer"></em>';
											switchHTML+='</div>';
											switchHTML+='<div class="col-md-8 no-padding">';
												switchHTML+='<strong class="title">'+data.result[r]['Name']+'</strong><br />';
												switchHTML+='<span class="state">'+data.result[r]['Data']+'</span>';
											switchHTML+='</div>';
										switchHTML+='</div>';
									switchHTML+='</div>';
								$('#switches').append(switchHTML);
							}
							else if(data.result[r]['Type']=='Wind'){
								$('.block'+data.result[r]['idx']).remove();
								var switchHTML = '<div class="col-md-4 mb no-pr" data-light="'+data.result[r]['idx']+'">';
										switchHTML+='<div class="transbg"> ';
											switchHTML+='<div class="col-md-4 no-padding">';
												switchHTML+='<em class="wi wi-wind-direction"></em>';
											switchHTML+='</div>';
											switchHTML+='<div class="col-md-8 no-padding">';
												switchHTML+='<strong class="title">'+data.result[r]['Name']+'</strong><br />';
												switchHTML+='<span class="state">'+data.result[r]['Gust']+'km/u, '+data.result[r]['DirectionStr']+'</span>';
											switchHTML+='</div>';
										switchHTML+='</div>';
									switchHTML+='</div>';
								$('#switches').append(switchHTML);
							}
							else{
								var buttonimg = 'bulb';
								if(data.result[r]['Image']=='Fan') buttonimg = 'fan';
								if(data.result[r]['Image']=='Heating') buttonimg = 'heating';

								if(data.result[r]['SwitchType']=='Dimmer'){
									var sliderHTML='<div class="col-md-12 no-pr">';
										sliderHTML+='<div class="transbg mb"> ';
											sliderHTML+='<div class="col-md-12 no-padding">';
												sliderHTML+='<strong class="title">'+data.result[r]['Name']+'</strong><br />';
											sliderHTML+='</div>';
											sliderHTML+='<div class="col-md-1 no-padding" data-light="'+data.result[r]['idx']+'" onclick="switchDevice(this);">';
													if(data.result[r]['Status']=='Off') sliderHTML+='<img src="img/'+buttonimg+'_off.png" class="off icon" />';
													else sliderHTML+='<img src="img/'+buttonimg+'_on.png" class="on icon" />';
												sliderHTML+='</div>';
											sliderHTML+='<div class="col-md-11">';
												sliderHTML+='<div class="slider slider'+data.result[r]['idx']+'" data-light="'+data.result[r]['idx']+'"></div>';
											sliderHTML+='</div>';
										sliderHTML+='</div>';
									sliderHTML+='</div>';
									$('#sliders').append(sliderHTML);

									$( ".slider"+data.result[r]['idx'] ).slider({
										value:Math.ceil((data.result[r]['Level']/100)*16),
										step: 1,
										min:2,
										max:16,
										slide: function( event, ui ) {
											sliding = true;
											slideDevice($(this).data('light'),ui.value);
										},
										stop: function( event, ui ) {
											sliding = false;
										}
									});
								}
								else if(data.result[r]['Type']=='Group'){
									var switchHTML = '<div class="col-md-4 mb no-pr" data-light="'+data.result[r]['idx']+'" onclick="switchGroup(this);">';
											switchHTML+='<div class="transbg"> ';
												switchHTML+='<div class="col-md-4 no-padding">';
													if(data.result[r]['Status']=='Off') switchHTML+='<img src="img/'+buttonimg+'_off.png" class="off icon" />';
													else switchHTML+='<img src="img/'+buttonimg+'_on.png" class="on icon" />';
												switchHTML+='</div>';
												switchHTML+='<div class="col-md-8 no-padding">';
													switchHTML+='<strong class="title">'+data.result[r]['Name']+'</strong><br />';
													if(data.result[r]['Status']=='Off') switchHTML+='<span class="state">UIT</span>';
													else switchHTML+='<span class="state">AAN</span>';
												switchHTML+='</div>';
											switchHTML+='</div>';
										switchHTML+='</div>';
									$('#groups').append(switchHTML);
								}
								else if(typeof(data.result[r]['LevelActions'])!=='undefined' && data.result[r]['LevelActions']!==""){
									var actions = data.result[r]['LevelActions'].split('|');
									var names = data.result[r]['LevelNames'].split('|');

									var switchHTML = '<div class="col-md-4 mb no-pr switch" data-light="'+data.result[r]['idx']+'">';
											switchHTML+='<div class="transbg"> ';
												switchHTML+='<div class="col-md-4 no-padding">';
													//if(parseFloat(data.result[r]['Level'])==0) switchHTML+='<img src="img/'+buttonimg+'_off.png" class="off icon" />';
													//else 
														switchHTML+='<img src="img/'+buttonimg+'_on.png" class="on icon" />';
												switchHTML+='</div>';
												switchHTML+='<div class="col-md-8 no-padding">';
													switchHTML+='<strong class="title">'+data.result[r]['Name']+'</strong><br />';
													switchHTML+='<select onchange="switchDeviceBtn(this,this.value);">';
													switchHTML+='<option value="">Selecteer</option>';
													for(a in actions){
														switchHTML+='<option value="'+actions[a]+'">'+names[a]+'</option>';
													}
													switchHTML+='</select>';
												switchHTML+='</div>';
											switchHTML+='</div>';
										switchHTML+='</div>';
									$('#switches').append(switchHTML);

								}
								else {
									var switchHTML = '<div class="col-md-4 mb no-pr" data-light="'+data.result[r]['idx']+'" onclick="switchDevice(this);">';
											switchHTML+='<div class="transbg"> ';
												switchHTML+='<div class="col-md-4 no-padding">';
													if(data.result[r]['Status']=='Off') switchHTML+='<img src="img/'+buttonimg+'_off.png" class="off icon" />';
													else switchHTML+='<img src="img/'+buttonimg+'_on.png" class="on icon" />';
												switchHTML+='</div>';
												switchHTML+='<div class="col-md-8 no-padding">';
													switchHTML+='<strong class="title">'+data.result[r]['Name']+'</strong><br />';
													if(data.result[r]['Status']=='Off') switchHTML+='<span class="state">UIT</span>';
													else switchHTML+='<span class="state">AAN</span>';
												switchHTML+='</div>';
											switchHTML+='</div>';
										switchHTML+='</div>';
									$('#switches').append(switchHTML);
								}
							}
						}
					}
				}
			}
		});
	}
}

function number_format (number, decimals, decPoint, thousandsSep) { // eslint-disable-line camelcase
  number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
  var n = !isFinite(+number) ? 0 : +number
  var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
  var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
  var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
  var s = ''
  var toFixedFix = function (n, prec) {
    var k = Math.pow(10, prec)
    return '' + (Math.round(n * k) / k)
      .toFixed(prec)
  }
  // @todo: for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
  }
  if ((s[1] || '').length < prec) {
    s[1] = s[1] || ''
    s[1] += new Array(prec - s[1].length + 1).join('0')
  }
  return s.join(dec)
}

function getIcon(code){
	console.log('wheater: '+code);
	var wiclass='wi-cloudy';
	
	if(code=='clear') 	wiclass="wi-day-sunny";
	if(code=='') 		wiclass="wi-day-cloudy";
	if(code=='') 		wiclass="wi-day-thunderstorm";
	if(code=='rain' || code=='chancerain') 	wiclass="wi-rain";
	if(code=='') 		wiclass="wi-rain-mix";
	if(code=='') 		wiclass="wi-showers";
	if(code=='cloudy' || code=='partlycloudy') 	wiclass="wi-cloudy";
	if(code=='') 		wiclass="wi-night-cloudy";
	if(code=='') 		wiclass="wi-night-clear";
	if(code=='') 		wiclass="wi-cloudy-gusts";
	if(code=='') 		wiclass="wi-tornado";
	if(code=='') 		wiclass="wi-storm-showers";
	if(code=='tstorms') wiclass="wi-thunderstorm";
	if(code=='snow') 	wiclass="wi-snow";
	if(code=='') 		wiclass="wi-hail";
	if(code=='') 		wiclass="wi-fog";
	return wiclass;
}

function setSrc(cur){
	if(typeof($($(cur).data('target')).find('iframe').attr('src'))=='undefined'){
		$($(cur).data('target')).find('iframe').attr('src',$($(cur).data('target')).find('iframe').data('src'));
	}
}