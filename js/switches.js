function switchDevice(cur){
	var idx = $(cur).data('light');
	if($(cur).find('img.icon').hasClass('on')){
		var doStatus='Off';
		$(cur).find('img.icon').removeClass('on');
		$(cur).find('img.icon').addClass('off');
		$(cur).find('img.icon').attr('src',$(cur).find('img.icon').attr('src').replace('on','off'));
		$(cur).find('.state').html('UIT');
	}
	else {
		var doStatus='On';
		$(cur).find('img.icon').removeClass('off');
		$(cur).find('img.icon').addClass('on');
		$(cur).find('img.icon').attr('src',$(cur).find('img.icon').attr('src').replace('off','on'));
		$(cur).find('.state').html('AAN');
	}
	
	if(typeof(req)!=='undefined') req.abort();
	$.ajax({
		url: _HOST_DOMOTICZ+'/json.htm?type=command&param=switchlight&idx='+idx+'&switchcmd='+doStatus+'&level=0&passcode=&jsoncallback=?',
		type: 'GET',async: false,contentType: "application/json",dataType: 'jsonp'
	});
}

function switchDeviceBtn(cur,url){
	/*var idx = $(cur).parents('.switch').data('light');
	if($(cur).parents('.switch').find('img.icon').hasClass('on')){
		var doStatus='Off';
		$(cur).parents('.switch').find('img.icon').removeClass('on');
		$(cur).parents('.switch').find('img.icon').addClass('off');
		$(cur).parents('.switch').find('img.icon').attr('src',$(cur).parents('.switch').find('img.icon').attr('src').replace('on','off'));
		$(cur).parents('.switch').find('.state').html('UIT');
	}
	else {
		var doStatus='On';
		$(cur).parents('.switch').find('img.icon').removeClass('off');
		$(cur).parents('.switch').find('img.icon').addClass('on');
		$(cur).parents('.switch').find('img.icon').attr('src',$(cur).parents('.switch').find('img.icon').attr('src').replace('off','on'));
		$(cur).parents('.switch').find('.state').html('AAN');
	}
	*/
	if(url!==""){
		if(typeof(req)!=='undefined') req.abort();	
		$.ajax({
			url: decodeURIComponent(url),
			type: 'GET',async: false,contentType: "application/json",dataType: 'jsonp'
		});
	}
}
function switchGroup(cur){
	var idx = $(cur).data('light');
	if($(cur).find('img.icon').hasClass('on')){
		var doStatus='Off';
		$(cur).find('img.icon').removeClass('on');
		$(cur).find('img.icon').addClass('off');
		$(cur).find('img.icon').attr('src',$(cur).find('img.icon').attr('src').replace('on','off'));
		$(cur).find('.state').html('UIT');
	}
	else {
		var doStatus='On';
		$(cur).find('img.icon').removeClass('off');
		$(cur).find('img.icon').addClass('on');
		$(cur).find('img.icon').attr('src',$(cur).find('img.icon').attr('src').replace('off','on'));
		$(cur).find('.state').html('AAN');
	}
	
	if(typeof(req)!=='undefined') req.abort();	
	$.ajax({
		url: _HOST_DOMOTICZ+'/json.htm?type=command&param=switchscene&idx='+idx+'&switchcmd='+doStatus+'&level=0&passcode=&jsoncallback=?',
		type: 'GET',async: false,contentType: "application/json",dataType: 'jsonp'
	});
}

function slideDevice(idx,status){
	if(typeof(slide)!=='undefined') slide.abort();
	slide = $.ajax({
		url: _HOST_DOMOTICZ+'/json.htm?type=command&param=switchlight&idx='+idx+'&switchcmd=Set%20Level&level='+status+'&jsoncallback=?',
		type: 'GET',async: false,contentType: "application/json",dataType: 'jsonp'
	});
}