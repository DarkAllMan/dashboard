function getTraffic(){
	//http://www.vid.nl/VI/overzicht
	var rssurl = 'http://www.vid.nl/VI/_rss';
	$("#traffic").html('');
	$("#traffic").rss(rssurl,{
		layoutTemplate: "{entries}",
		entryTemplate: '<div class="col-md-12 no-padding trafficrow" data-toggle="modal" data-target="#trafficweb" onclick="setSrc(this);"><div class="transbg"><strong>{title}</strong><br/>{shortBodyPlain}</div></div>',
		success: function(){ 
			if($('.trafficrow').length==0){ 
				$('.containstraffic').hide(); 
			}
			else {
				var html = '<div class="modal fade" id="trafficweb" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
				  html+='<div class="modal-dialog">';
					html+='<div class="modal-content">';
					  html+='<div class="modal-header">';
						html+='<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
						html+='<h4 class="modal-title" id="myModalLabel2">Verkeer</h4>';
					  html+='</div>';
					  html+='<div class="modal-body">';
						  html+='<iframe data-src="http://www.vid.nl/VI/overzicht" width="100%" height="570" frameborder="0" allowtransparency="true"></iframe> '; 
					  html+='</div>';
					html+='</div>';
				  html+='</div>';
				html+='</div>';
				$('header').append(html);
			}
		},
		error: function(){ $('.containstraffic').hide(); }
	});
	
}