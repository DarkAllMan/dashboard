

function loadWeather(location,country) {
	var html = '';
	
	$.getJSON('http://api.wunderground.com/api/'+_APIKEY_WUNDERGROUND+'/conditions/q/'+country+'/'+location+'.json',function(weather){
		
		
		if(typeof(weather.current_observation)=='undefined'){
			$('.containsweather').remove();
			currentweather=false;
			$("#weather").html('<p style="font-size:10px; width:100px;">Location ERROR</p>');
		}
		else {
			currentweather = weather.current_observation;
			var wiclass= getIcon(currentweather.icon);
			
			html += '<h2><span>'+Math.round(currentweather.temp_c)+'°C</span> <i class="wi '+wiclass+'"></i></h2>';
			$("#weather").html('<i class="wi '+wiclass+'"></i>');
			$("#weatherdegrees").html('<strong>'+Math.round(currentweather.temp_c)+'°C</strong><span class="rainrate"></span>');
			$("#weatherloc").html(location);
		}
		
	});
}

function loadWeatherFull(location,country) {
	var html = '';
	$.getJSON('http://api.wunderground.com/api/'+_APIKEY_WUNDERGROUND+'/forecast10day/q/'+country+'/'+location+'.json',function(currentforecast){
		
		$("#weatherfull ul li").html('');

		var day;
		for(var i=1;i<5;i++) {
			curfor = currentforecast.forecast.simpleforecast.forecastday[i];
			day = curfor.date.weekday_short;
		//	console.log(curfor);
			switch(day) {
				case 'Mon': dayNL = 'Maandag'; break;
				case 'Tue': dayNL = 'Dinsdag'; break;
				case 'Wed': dayNL = 'Woensdag'; break;
				case 'Thu': dayNL = 'Donderdag'; break;
				case 'Fri': dayNL = 'Vrijdag'; break;
				case 'Sat': dayNL = 'Zaterdag'; break;
				case 'Sun': dayNL = 'Zondag'; break;
			}

			var wiclass = getIcon(curfor.icon);

			html = '<div class="day">'+dayNL.toLowerCase()+'<br />'+curfor.date.day+'/'+curfor.date.month+'</div>';
			html += '<div class="icon"><i class="wi '+wiclass+'"></i></div>';
			html += '<div class="temp"><span class="dayT">'+curfor.high.celsius+'°C</span><span class="nightT">'+curfor.low.celsius+'°C</span></div>';

			$('#weatherfull ul li:eq('+(i-1)+')').html(html);
		}
	});
	
	
}